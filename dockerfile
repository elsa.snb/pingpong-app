FROM node
WORKDIR /app
COPY . .
RUN yarn install \
 && mv .env.example .env
CMD ["yarn", "run", "start"]
EXPOSE 8080